<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\Principle\AddValidation;
use App\Http\Requests\Principle\EditValidation;
use App\Models\Principle;
use Illuminate\Http\Request;


class PrincipleController extends AdminBaseController
{
    protected $base_route = 'admin.principle';
    protected $view_path = 'admin.principle';
    protected $view_title = 'Principle Manger';
    protected $folder_name = 'principle';
    protected $trans_path = 'banner';
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function add(Request $request)
    {

        $data = [];
        $data['row'] = Principle::select()->first();

        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function update(EditValidation $request, $id)
    {

        if (!$listing = Principle::find($id)){

            if ($request->hasFile('image')){
                $image = $request->file('image');
                $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
                $image->move($this->folder_path,$file_name);
            }else{
                $file_name = $request->get('oldimg');
            }

            $list = Principle::create([
                'title'               =>     $request->get('title'),
                'slug'               =>     str_slug($request->get('title')),
                'image'                =>    $file_name,
                'message'          =>     $request->get('message'),
                'status' => $request->get('status'),
            ]);
        }else{



            if ($request->hasFile('image')){
                $image = $request->file('image');
                $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
                $image->move($this->folder_path,$file_name);

//                if(isset($listing->image) && !empty($listing->image))
//                    unlink($this->folder_path.$listing->image);

            }else{
                $file_name = $request->get('oldimg');
            }

            $listing->update([
                'title'               =>     $request->get('title'),
                'slug'               =>     str_slug($request->get('title')),
                'image'                =>    $file_name,
                'message'          =>     $request->get('message'),
                'status' => $request->get('status'),
            ]);
        }

        $request->session()->flash('message', 'Principle updated successfully.');
        return redirect()->route($this->base_route.'.add');
    }

    public function imageDelete(Request $request, $id)
    {

        $type = $request->get('type');
        $value = $request->get('value');

        $response = [];

        $response['error'] = true;

        if (!$listing = Principle::find($id))
            $response['message'] = 'data not found';


        // remove image before deleting db row

        /*if($listing->image ==! '')
            unlink($this->folder_path.$listing->image);*/

        switch ($type){
            case image:
                $listing->update([
                    'image'                =>    null,
                ]);
                break;
            case title:
                $listing->update([
                    'title'                =>    $value,
                ]);
                break;


        }

        if(isset($listing->image))
            unlink($this->folder_path.$listing->image);

        $response['error'] = false;

        return response()->json(json_encode($response));

    }



}