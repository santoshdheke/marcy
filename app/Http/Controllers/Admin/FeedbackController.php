<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\Feedback\AddValidation;
use App\Http\Requests\Feedback\EditValidation;
use App\Models\Feedback;
use Illuminate\Http\Request;


class FeedbackController extends AdminBaseController
{
    protected $base_route = 'admin.feedback';
    protected $view_path = 'admin.feedback';
    protected $view_title = 'Feedback Manger';
    protected $trans_path = 'Feedback Manger';


    public function index(Request $request)
    {

        $data = [];
        $data['rows'] = Feedback::select('id','name', 'message','status')
            ->orderBy('id')
            ->paginate(30);

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(AddValidation $request)
    {

        $list = Feedback::create([
            'name' => $request->get('title'),
            'role' => null,
            'email' => $request->get('email'),
            'image' => null,
            'message' => $request->get('description'),
            'status' => $request->get('status')
        ]);

        $request->session()->flash('message', 'Feedback added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        //dd($id);
        $data = [];

        if (!$data['santosh'] = Feedback::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        $data['row'] = Feedback::find($id);

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(EditValidation $request, $id)
    {

        if (!$listing = Feedback::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
            if($listing->image ==! '')
                unlink($this->folder_path.$listing->image);
        }else{
            $file_name = $request->get('oldimg');
        }

        $listing->update([
            'name' => $request->get('title'),
            'role' => null,
            'email' => $request->get('email'),
            'image' => null,
            'message' => $request->get('description'),
            'status' => $request->get('status')
        ]);




        $request->session()->flash('message', 'Feedback updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
        //dd('enter');
        if (!$listing = Feedback::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $listing->delete();
        if($listing->image ==! '')
            unlink($this->folder_path.$listing->image);
        $request->session()->flash('message', 'Feedback deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}