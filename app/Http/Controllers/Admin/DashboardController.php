<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Report;

class DashboardController extends AdminBaseController
{
    protected $base_route = 'dashboard';
    protected $view_path = 'admin.dashboard';
    protected $view_title = 'Dashboard Manger';
    protected $trans_path = '';

    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path.'.home'));
    }

    public function error($code = '500')
    {
       $view_path = 'admin.error.'.$code;

        if (view()->exists($view_path)) {
            return view($view_path);
        }

        return view('admin.error.500');
    }
    
}