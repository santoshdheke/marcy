<?php

namespace App\Http\Controllers\WebPage;

use App\Models\AboutUs;
use App\Models\Banner;
use App\Models\Gallery;
use App\Models\News;
use App\Models\Page;
use App\Models\Photo;
use App\Models\Principle;
use Illuminate\Http\Request;

class WebPageController extends ComposerController
{
    protected $view_path = 'frontend.home';

    public function index(){

        $data = [];
        $data['banner'] = Banner::select()->where('title','banner')->first();
        $data['principle'] = Principle::select()->first();

        return view(parent::commonfunction($this->view_path.'.index'), compact('data'));

    }

    public function gallery()
    {
        $data = [];
        $data['gallery'] = Gallery::select('title', 'slug', 'image', 'status')
            ->limit(8)
            ->get();
        $data['photo'] = Photo::select('title', 'gallery_id', 'image', 'status')->get();

        return view(parent::commonfunction($this->view_path.'.gallery'), compact('data'));
        
    }

    public function about()
    {
        $data['aboutus'] = AboutUs::select('id','title', 'image', "description", 'status')
            ->limit(7)
            ->where('status', 1)
            ->get();
        $data['principle'] = Principle::select()->first();

        return view(parent::commonfunction($this->view_path.'.about'), compact('data'));

    }

    public function contact()
    {

        $data = [];
        $data['gallery'] = Gallery::select('title', 'image', 'slug','status')
            ->limit(8)
            ->get();

        return view(parent::commonfunction($this->view_path.'.contact'), compact('data'));
    }

    public function photo($slug)
    {
        $data = [];
        $data['photo'] = Photo::select('title', 'image', 'status')
            ->limit(8)
            ->where('gallery_id', $slug)
            ->get();

        return view(parent::commonfunction($this->view_path.'.photo'), compact('data'));

    }

    public function news($slug)
    {
        $data = [];
        $data['news'] = News::select()->where('slug', $slug)->first();

        return view(parent::commonfunction($this->view_path.'.news'), compact('data'));
    }

    public function feedback(Request $request)
    {

        dd($request->all());

    }

    public function newPage($slug)
    {
        $data = [];
        $data['page'] = Page::select()->where('slug','=',$slug)->first();


        switch ($slug){
            case 'gallerys':
                return redirect()->route('gallery');
            case 'contactus':
                return redirect()->route('contact');
            default :
                return view(Parent::commonfunction($this->view_path.'.page'),compact('data'));

        }
    }


}
