<?php

use Illuminate\Database\Seeder;

class aboutusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('about_us')->insert([
            'title' => 'Main',
            'slug' => 'main',
            'image' => '',
            'description' => '',
            'status' => 1,
        ]);
    }
}
