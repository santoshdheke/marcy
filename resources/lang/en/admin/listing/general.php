<?php
return [
    'menu' => [
        'manager' => 'Listing Manager',
        'list' => 'List',
        'add' => 'Add',
    ],
    'message' => [

    ],
    'buttons' => [

    ],
    'content' => [
        'common' => [
            'address' => 'Address',
            'banner' => 'Banner',
            'city' => 'City',
            'created_at' => 'Created At',
            'contact' => 'Contact',
            'description' => 'Description',
            'in_active' => 'In Active',
            'menu' => 'Menu',
            'prname' => 'Principal Name',
            'prmobile' => 'Principal Mobile',
            'status' => 'Status',
            'state' => 'State',
            'sn' => 'SN',
            'updated_at' => 'Updated At',


            'active' => 'Active',
            'add' => 'Add',
            'edit' => 'Edit',
            'email' => 'Email',
            'main_image'      =>  'Main Image',
            'manager' => 'Listing Manager',
            'name' => 'Name',
            'seo_description' => 'SEO Description',
            'seo_keyword' => 'SEO Keyword',
            'seo_title' => 'SEO Title',
            'slug' => 'URL',
            'short_desc' => 'Short Description',
        ],
        'list' => [
            'list' => 'List',
            'user_list' => 'User List',
        ],
        'add' => [
            'add_form' => 'Add Form',
        ],
        'edit' => [
            'edit_form' => 'Edit Form',
            'existing_banner' => 'Existing Banner',
        ],
        'delete' => [],
    ],
];