<style>
    .input_feld {
        width: 100%;
    }

    .input_div {
        margin-bottom: 20px;
    }

    .input_feld,
    .input_btn {
        padding: 5px 15px;
    }
</style>

<div class="advantages" style="background-color: #eeeeee;padding-top: 30px;">
    <div class="container">
        <div class="advantages-main">
            <div class="portfolio-top wow fadeInDown" style="margin-bottom: 50px;" data-wow-delay="0.3s">
                <h3>Testonomils</h3>
                <span class="heading-line"> </span>
                <p>Nemo enim ipsam voluptatem quia.</p>
            </div>

            @if(isset($data['feedback']) && count($data['feedback'])>0)

                <div class="carousel slide" id="carousel-example-captions" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">

                        @php($counter=0)
                        @foreach($data['feedback'] as $feedback)
                            @php($counter++)

                            <div class="item {{ $counter==1?'active':'' }}">
                                <div class="advantage-rit wow fadeInRight" data-wow-delay="0.3s" style="width: 50%;margin: 0 auto;">
                                    <div class="advant-layer1">
                                        <div class="adv-layer1-text">
                                            <h6>{{ $feedback->name }}</h6>
                                            <p>Co-founder</p>
                                        </div>
                                        <div class="advater-img">
                                            <img src="{{ asset('frontend/images/s2.png') }}" alt="">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="advant-layer2">
                                        <div class="adv-layer2-text">
                                            <img src="{{ asset('frontend/images/left.png') }}" alt="">
                                            <p>Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
                                        </div>
                                        <div class="advater-img">
                                            <img src="{{ asset('frontend/images/arrow2.png') }}" alt="">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                        </div>

                        @endforeach

                        {{--<div class="item">




                                                        <div class="advantage-rit wow fadeInRight" data-wow-delay="0.3s" style="width: 50%;margin: 0 auto;">
                                                            <div class="advant-layer1">
                                                                <div class="adv-layer1-text">
                                                                    <h6>Malorum</h6>
                                                                    <p>Co-founder</p>
                                                                </div>
                                                                <div class="advater-img">
                                                                    <img src="{{ asset('frontend/images/s2.png') }}" alt="">
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="advant-layer2">
                                                                <div class="adv-layer2-text">
                                                                    <img src="{{ asset('frontend/images/left.png') }}" alt="">
                                                                    <p>Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
                                                                </div>
                                                                <div class="advater-img">
                                                                    <img src="{{ asset('frontend/images/arrow2.png') }}" alt="">
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>



                        </div>
                        <div class="item">




                                                        <div class="advantage-rit wow fadeInRight" data-wow-delay="0.3s" style="width: 50%;margin: 0 auto;">
                                                            <div class="advant-layer1">
                                                                <div class="adv-layer1-text">
                                                                    <h6>Malorum</h6>
                                                                    <p>Co-founder</p>
                                                                </div>
                                                                <div class="advater-img">
                                                                    <img src="{{ asset('frontend/images/s2.png') }}" alt="">
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="advant-layer2">
                                                                <div class="adv-layer2-text">
                                                                    <img src="{{ asset('frontend/images/left.png') }}" alt="">
                                                                    <p>Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
                                                                </div>
                                                                <div class="advater-img">
                                                                    <img src="{{ asset('frontend/images/arrow2.png') }}" alt="">
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>



                        </div>--}}
                    </div>
                    <a href="#carousel-example-captions" class="left carousel-control" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" style="color: #000000;background-color: #ffffff;margin: 10px;width: 50px;height: 50px;padding: 10px;border-radius: 100%;"></span> <span class="sr-only">Previous</span>
                    </a> <a href="#carousel-example-captions" class="right carousel-control" role="button"
                            data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true" style="color: #000000;background-color: #ffffff;margin: 10px;width: 50px;height: 50px;padding: 10px;border-radius: 100%;"></span>
                        <span class="sr-only">Next</span> </a></div>

                @endif

        </div>
    </div>
</div>