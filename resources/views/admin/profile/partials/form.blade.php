<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

    <div class="col-sm-9">
        <input type="text" name="title" id="title" value="{{ ViewHelper::getData('title', isset($data['row'])?$data['row']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

@if(isset($data['row']['image']) && !empty($data['row']['image']))

    <div class="form-group" id="logo_image_div">
        <label class="col-sm-3 control-label no-padding-right" for="image"> Old Logo </label>

        <lable class="col-sm-9">

            <img src="{{ asset('images/profile/'.$data['row']->image) }}" width="200px" for="image" alt="">
            <a href="#" id="delete_logo">
                <i class="icon-remove"></i>
            </a>
        </lable>
    </div>
    <div class="space-4"></div>

    <input type="hidden" id="old_img" value="{{ $data['row']['image'] }}" name="oldimg">
@endif


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="image"> Logo </label>

    <div class="col-sm-9">
        <input type="file" name="image" id="image" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="country"> Country </label>

    <div class="col-sm-9">
        <input type="text" name="country" id="country" value="{{ ViewHelper::getData('country', isset($data['row'])?$data['row']:[]) }}" placeholder="Country" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="city"> City </label>

    <div class="col-sm-9">
        <input type="text" name="city" id="city" value="{{ ViewHelper::getData('city', isset($data['row'])?$data['row']:[]) }}" placeholder="City" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="phone"> Phone </label>

    <div class="col-sm-9">
        <input type="text" name="phone" id="phone" value="{{ ViewHelper::getData('phone', isset($data['row'])?$data['row']:[]) }}" placeholder="Phone" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="mobile"> Mobile </label>

    <div class="col-sm-9">

        <input type="text" name="mobile" id="mobile" value="{{ ViewHelper::getData('mobile', isset($data['row'])?$data['row']:[]) }}" placeholder="Mobile" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="email"> Email </label>

    <div class="col-sm-9">
        <input type="email" name="email" id="email" value="{{ ViewHelper::getData('email', isset($data['row'])?$data['row']:[]) }}" placeholder="Email" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="facebook"> Facebook </label>

    <div class="col-sm-9">
        <input type="url" name="facebook" id="facebook" value="{{ ViewHelper::getData('facebook', isset($data['row'])?$data['row']:[]) }}" placeholder="Facebook" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="twitter"> Twitter </label>

    <div class="col-sm-9">
        <input type="url" name="twitter" id="twitter" value="{{ ViewHelper::getData('twitter', isset($data['row'])?$data['row']:[]) }}" placeholder="Twitter" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="google"> Google + </label>

    <div class="col-sm-9">
        <input type="url" name="google" id="google" value="{{ ViewHelper::getData('google', isset($data['row'])?$data['row']:[]) }}" placeholder="Google +" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="skype"> Skype </label>

    <div class="col-sm-9">
        <input type="url" name="skype" id="skype" value="{{ ViewHelper::getData('skype', isset($data['row'])?$data['row']:[]) }}" placeholder="Skype" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="linkedin"> Linked In </label>

    <div class="col-sm-9">
        <input type="url" name="linkedin" id="linkedin" value="{{ ViewHelper::getData('linkedin', isset($data['row'])?$data['row']:[]) }}" placeholder="Linked In" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>


<script>
    $(document).ready(function () {
        $('#delete_logo').click(function () {
            $.ajax({

                method : 'POST',
                data : {
                    '_token' : '{{ csrf_token() }}'
                },
                url : '{{ route('admin.profile.delete',$data['row']['id']) }}',
                success : function (responce) {

                    var data = $.parseJSON(responce);

                    if (data.error){

                    }else{
                        $('#logo_image_div').hide(500);
                        $('#old_img').val(null);
                    }

                }

            });
        });
    });
</script>