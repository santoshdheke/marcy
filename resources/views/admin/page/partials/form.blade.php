<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

    <div class="col-sm-9">
        <input type="text" name="title" id="title" value="{{ ViewHelper::getData('title', isset($data['row'])?$data['row']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

@if(!isset($data['row']))
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="slug"> Slug </label>

        <div class="col-sm-9">
            <input type="text" name="slug" id="slug" value="{{ ViewHelper::getData('slug', isset($data['row'])?$data['row']:[]) }}" placeholder="Slug" class="col-xs-10 col-sm-5">
        </div>
    </div>
    <div class="space-4"></div>
@endif

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="description"> Description </label>

    <div class="col-sm-9">
        <textarea name="description" id="description" placeholder="Title" class="col-xs-10 col-sm-5">{{ ViewHelper::getData('description', isset($data['row'])?$data['row']:[]) }}</textarea>
    </div>
</div>
<div class="space-4"></div>

<div class="form-group" id="parent_menu_hide_gar">
    <label class="col-sm-3 control-label no-padding-right" for="parent_menu"> Page Link </label>

    <div class="col-sm-9">
        <select name="page_link" id="parent_menu" class="col-xs-10 col-sm-5">
                <option value="index" {!! (isset($data['row']->page_link) && $data['row']->page_link=='index')?'selected':'' !!}>Home</option>
                <option value="gallery" {!! (isset($data['row']->page_link) && $data['row']->page_link=='gallery')?'selected':'' !!}>Gallery</option>
                <option value="about" {!! (isset($data['row']->page_link) && $data['row']->page_link=='about')?'selected':'' !!}>About Us</option>
                <option value="contact" {!! (isset($data['row']->page_link) && $data['row']->page_link=='contact')?'selected':'' !!}>Contact Us</option>
                <option value="newss" {!! (isset($data['row']->page_link) && $data['row']->page_link=='newss')?'selected':'' !!}>News</option>
                <option value="simplepage" {!! (isset($data['row']->page_link) && $data['row']->page_link=='simplepage')?'selected':'' !!}>Simple Page</option>
                <option value="alumni" {!! (isset($data['row']->page_link) && $data['row']->page_link=='alumni')?'selected':'' !!}>Alumni</option>
                <option value="careers" {!! (isset($data['row']->page_link) && $data['row']->page_link=='careers')?'selected':'' !!}>Careers</option>
        </select>

    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="status"> Status </label>

    <div class="col-sm-9">
        <div class="radio">
            <label>
                <input name="status" value="1" {{ (isset($data['row'])&&$data['row']->status==1)?'checked':'' }} {{ isset($data['row'])?'':'checked' }} type="radio" class="ace">
                <span class="lbl"> Active</span>
            </label>
        </div>
        <div class="radio">
            <label>
                <input name="status" {{ (isset($data['row'])&&$data['row']->status==0)?'checked':'' }} value="0" type="radio" class="ace">
                <span class="lbl"> In-active</span>
            </label>
        </div>
    </div>
</div>
<div class="space-4"></div>


<script>

    $(document).ready(function () {

        $('#title').keyup(function () {
            var title = $('#title').val();
            $('#slug').val(title.replace(/\s+/g, '-').toLowerCase());
        });

    });

</script>