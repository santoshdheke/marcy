@extends('admin.common.layout.layout')

@section('page_title'){{ $view_title }} - {{ trans('general.admin_panel') }} @endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('menu/general.menu.manager') }}</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.index') }}">{{ $view_title }} </a>
                </li>

                @if (isset($data))
                    <li class="active">{{ trans('admin/fmenu/list.top.edit') }}</li>
                @else
                    <li class="active">{{ trans('admin/fmenu/list.top.add') }}</li>
                @endif

            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off">
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $view_title }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans('admin/fmenu/list.top.list') }}
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">

                                @if (Request::session()->has('message'))
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! Request::session()->get('message') !!}
                                        <br>
                                    </div>
                                @endif

                                <h1>menu</h1>
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            sn
                                        </th>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Sub Menu</th>
                                        <th>status</th>
                                        <th>Page</th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if ($data['menu']->count() > 0)
                                        @php $counter=0; @endphp
                                        @foreach($data['menu'] as $row)
                                            @php $counter++ @endphp

                                            <tr>

                                                <td class="center">
                                                    {{ $counter }}
                                                </td>

                                                <td>
                                                    {{ $row->id }}
                                                </td>

                                                <td>
                                                    {{ $row->title }}
                                                </td>

                                                <td>
                                                    @if($data['sub-menu'])

                                                        @foreach($data['sub-menu'] as $rows)

                                                            {!! ($rows->parent_menu==$row->id)?'* '.$rows->title.' <a href="'.route($base_route.'.edit', $rows->id).'"> edit</a> / <a href="'.route($base_route.'.delete', $rows->id).'">delete</a><br>':'' !!}

                                                            @endforeach
                                                        @endif
                                                </td>

                                                <td>
                                                    @if ($row->status == 1)
                                                        Active
                                                    @else
                                                        InActive
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($row->page_id == null)
                                                        <a href="#" class="btn btn-xs btn-danger">No Link</a>
                                                        @else
                                                        <a href="#" class="btn btn-xs btn-success">{{ $row->Page->title }}</a>
                                                        @endif

                                                </td>



                                                @if(Auth::user()->type=="admin")
                                                    <td>
                                                        <div class="btn-group">

                                                            <a class="btn btn-xs btn-info" href="{{ route($base_route.'.edit', ['id' => $row->id]) }}">
                                                                <i class="icon-edit bigger-120"></i>
                                                            </a>



                                                        </div>

                                                        <div class="btn-group">

                                                            <a href="{{ route($base_route.'.delete', $row->id) }}"  class="btn btn-xs btn-danger bootbox-confirm">
                                                                <i class="icon-trash bigger-120"></i>
                                                            </a>



                                                        </div>


                                                    </td>
                                                @endif
                                            </tr>

                                        @endforeach

                                    @else

                                        <tr>
                                            <td colspan="7">No data found.</td>
                                        </tr>

                                    @endif

                                    </tbody>
                                </table>
                                    <div style="height: 20px;"></div>
                                    <h1>sub menu</h1>
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            sn
                                        </th>
                                        <th>Name</th>
                                        <th>Parent Menu</th>
                                        <th>status</th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if ($data['sub-menu']->count() > 0)
                                        @php $counter=0; @endphp
                                        @foreach($data['sub-menu'] as $row)
                                            @php $counter++ @endphp

                                            <tr>

                                                <td class="center">
                                                    {{ $counter }}
                                                </td>

                                                <td>
                                                    {{ $row->title }}
                                                </td>

                                                <td>
                                                    {{ $row->parent->title }}
                                                </td>

                                                <td>
                                                    @if ($row->status == 1)
                                                        Active
                                                    @else
                                                        InActive
                                                    @endif
                                                </td>



                                                @if(Auth::user()->type=="admin")
                                                    <td>
                                                        <div class="btn-group">

                                                            <a class="btn btn-xs btn-info" href="{{ route($base_route.'.edit', ['id' => $row->id]) }}">
                                                                <i class="icon-edit bigger-120"></i>
                                                            </a>



                                                        </div>

                                                        <div class="btn-group">

                                                            <a href="{{ route($base_route.'.delete', $row->id) }}"  class="btn btn-xs btn-danger bootbox-confirm">
                                                                <i class="icon-trash bigger-120"></i>
                                                            </a>



                                                        </div>


                                                    </td>
                                                @endif
                                            </tr>

                                        @endforeach

                                    @else

                                        <tr>
                                            <td colspan="7">No data found.</td>
                                        </tr>

                                    @endif

                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-4">

                            </div>

                            <!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

@endsection

@section('page_specific_scripts')

    <script>
        $.ajax({
            type : 'post',
            url : ''
        });
    </script>

@endsection